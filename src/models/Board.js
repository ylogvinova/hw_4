class Board {
  constructor() {
    this.board = ''
    this.layout = []
    this.count = 9
  }

  // helpers
  displayItem(item){
    return item === undefined ? ' ' : item
  }

  /// update layout
  updateLayout(){
    let boardCell = ``;

    for (let i = 0; i < this.count; i++) {
      boardCell = boardCell + `${this.displayItem(this.layout[i] || i + 1)} `
      if (i === 2 || i === 5) {
        boardCell = boardCell + `\n-----\n`
      }
    }

    this.board = boardCell

    console.log(this.board);
  }

  getLayout() {
    return this.layout
  }

  getLayoutByIndex(index) {
    return this.layout[index]
  }

  setLayout(index, value) {
    this.layout[index] = value
  }
}

module.exports = Board
