const char = require('../constants')


class User {
  constructor(name, character) {
    this.player_name = name;
    this.player_character = character;
  }
}

class Players {
  constructor() {
    this.user_1 = null
    this.user_2 = null
    this.currentPlayer = null
  }

  setupPlayers (name1, name2) {
    this.user_1 = new User(name1, char.x)
    this.user_2 = new User(name2, char.o)

    this.currentPlayer = this.user_1
  }

  getCurrentPlayer() {
    return this.currentPlayer
  }

  setCurrentPlayer (character) {
    this.currentPlayer = character === char.x ? this.user_2: this.user_1
  }

  getPlayerByCharacter (character) {
    return character === char.x ? this.user_1: this.user_2
  }
}

module.exports = Players
