const readline = require('readline')

const Analytics = require('./Analytics')
const Board = require('./Board')
const Players = require('./Players')

class Game {
  constructor(){
    this.gameEnded = false;
    this.gameAnalytics = new Analytics()
    this.board = new Board()
    this.players = new Players()

    this.winnerCombinations = [
      [0,3,6], [1,4,7], [2,5,8], // vertical
      [0,1,2], [3,4,5], [6,7,8], // horizontal
      [0,4,8], [2,4,6]           // diagonal
    ]

    this.rl = readline.createInterface({
      input: process.stdin,
      output: process.stdout
    })
  }

  startGame() {

    //TODO: Allow players to write their names
    this.players.setupPlayers('player_1', 'player_2')

    console.log("Game started");
    this.board.updateLayout();

    // listen to inputs
    this.rl.on("line", (input) => {
      const layout = this.board.getLayout()

      if (layout.length <= 9){
        // read move
        this.readMove(parseInt(input))
      } else {
        console.log("Game Ended!");
        this.processGame();
      }
    })
  }

  endGame(){
    this.rl.close();
    console.log("--- Game history ---")

    this.gameEnded = true;

    //TODO: Read analytics from analytics.json
    const analytics = this.gameAnalytics.getAnalytics()
    analytics.forEach(an => {
      console.log(`${an.player} use character ${an.character} made step ${an.step}`)
    })

    process.exit();
  }

  continuePlay() {
    if(!this.gameEnded){
      const currentPlayer = this.players.getCurrentPlayer();
      // switch player
      this.setPlayerFromChar(currentPlayer.player_character)

      console.log(`${currentPlayer.player_name}, please select position 1-9: `)

      this.board.updateLayout();
      this.processGame();
    }
  }

  processGame(){
    const history = this.gameAnalytics.getAnalytics()
    const _this = this

    if (history.length < 9){
      let setSteps = new Set()

      this.winnerCombinations.forEach(combination => {
        const item_1 = _this.board.getLayoutByIndex(combination[0])
        const item_2 = _this.board.getLayoutByIndex(combination[1])
        const item_3 = _this.board.getLayoutByIndex(combination[2])

        // check all characters on equal
        const createArray = Array.from(setSteps.add(item_1).add(item_2).add(item_3))

        // if layouts are exist and they have the same value
        if (item_1 && item_2 && item_3 && createArray.length === 1){
          console.log(`${_this.players.getPlayerByCharacter(item_1).player_name} you're the winner`);
          _this.endGame();
        }

        setSteps.clear();
      })
    }

    // if we have 9 moves, and no one wins
    if (history.length >= 9) {
      console.log(`Friendship Always Wins`);

      this.endGame();
    }
  }

  setPlayerFromChar(character){
    return this.players.setCurrentPlayer(character)
  }

  readMove(index){
    const currentPlayer = this.players.getCurrentPlayer()

    // if player passed wrong index
    if (!index || (index > 9) || index < 1) {
      console.log("---- wrong position----")
      return this.stepError();
    }

    // if index is used by another player
    if (this.board.getLayoutByIndex(index - 1) !== undefined){
      console.log("---- Position is Used ----")
      return this.stepError();
    } else {
      this.board.setLayout((index - 1), currentPlayer.player_character)
      this.recordMove((index - 1), currentPlayer.player_character);
      this.continuePlay();
    }
  }

  // wrong
  stepError(){
    const currentPlayer = this.players.getCurrentPlayer();
    console.log(`${currentPlayer.player_name}, select position [1-9]: `)
  }

  recordMove(position, character){
    const currentPlayer = this.players.getCurrentPlayer();
    this.gameAnalytics.setAnalytics(position, character, currentPlayer.player_name)
  }
}

module.exports = Game
