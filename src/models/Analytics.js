const fs = require('fs')
const path = require('path')

class Analytics {
  constructor() {
    this.gameAnalytics = [];
  }

  setAnalytics(position, currentSymbol, currentPlayer) {
    this.gameAnalytics.push({
      step: position,
      character: currentSymbol,
      player: currentPlayer
    });

    fs.writeFileSync(path.join(__dirname, `../../analytics.json`), JSON.stringify(this.gameAnalytics, null, 2))
  }

  getAnalytics() {
    return this.gameAnalytics
  }
}

module.exports = Analytics
